import numpy as np
#import matplotlib as plt
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

import astropy.units as u

from astropy.wcs import WCS

def plot_FluxMap(moment_0, norm=1E-20, cmap='RdBu_r', scale =''):
    moment_0_norm = moment_0 / norm
    min = moment_0_norm[np.where(moment_0_norm >0)].min()
    # Initiate a figure and axis object with WCS projection information
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111, projection=moment_0_norm.wcs)
    # Display the moment map image
    if scale == 'LogNorm':
        im = ax.imshow(moment_0_norm.hdu.data, cmap=cmap,vmin=min.value, norm=LogNorm(), origin='lower')
    else :
        im = ax.imshow(moment_0_norm.hdu.data, cmap=cmap,vmin=min.value,  origin='lower')

    # Add axes labels
    ax.set_xlabel("'Right Ascension (J2000)", fontsize=14)
    ax.set_ylabel("Declination (J2000)", fontsize=10)

    # Add a colorbar
    cbar = plt.colorbar(im, pad=.07)
    cbar.set_label('Flux x 1E-20 (erg/s/cm2)', size=10)

    # Overlay set of RA/Dec Axes
    overlay = ax.get_coords_overlay('fk5')
    overlay.grid(color='white', ls='dotted', lw=2)


def plot_velocity(m_vel, vel_min_scale =-130,vel_max_scale = 255):    
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    im = ax.imshow(m_vel, vmin=vel_min_scale, vmax=vel_max_scale, origin='lower', cmap= 'RdBu_r')
    cbar = plt.colorbar(im, pad=.07)
    cbar.set_label('km/s', size=10)

def plot_sigma(sigma_map, sigma_min_scale = 1.0, sigma_max_scale = 4.5 ):    
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    im = ax.imshow(sigma_map, vmin=sigma_min_scale, vmax=sigma_max_scale, origin='lower', cmap= 'RdBu_r')
    cbar = plt.colorbar(im, pad=.07)
    cbar.set_label('km/s', size=10)

def plot_flux(Flux_map, flux_min_scale = 0.0001, flux_max_scale = 0.1 ):    
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    im = ax.imshow(Flux_map, vmin=flux_min_scale, vmax=flux_max_scale, origin='lower', cmap= 'viridis', norm=LogNorm())
    cbar = plt.colorbar(im, pad=.07)
    cbar.set_label('Flux', size=10)