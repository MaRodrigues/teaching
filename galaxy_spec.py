# Functions for spectral analysis of galaxy spectra

from matplotlib import pyplot as plt
from spectral_cube import SpectralCube
from astropy.modeling import models, fitting
import math 
from astropy.io import fits

import numpy as np

def Plot_spectrum_lines(wave, flux, lines_list):
    fig = plt.figure(figsize=(12, 4))
    p = plt.plot(wave, flux, label="data")
    p = plt.xlabel("Wavelength")
    p = plt.ylabel("Flux")
    for line in lines_list:
        t = plt.axvline(line['lambda'],label=line['name'], linestyle='dashed',color='k')
    p = plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')

    plt.show()

def Aperture_extract(cube, center, aper, save = True, file="aperture.fits"): 
    # to complete with coordinates units etc..
    dec, ra = Full_cube.world[1:2]  
    radius = ((dec-center[0])**2 + (ra-center[1])**2)**0.5  
    mask = radius <= aper  
    maskedcube = cube.with_mask(mask)  
    spectrum = maskedcube.mean(axis=(1,2))  
    if save : 
        spectrum.write(file)
    return spectrum       
    
def Collpase_allSpatial(cube, save = True, file="integrated.fits" ):    
    cube.allow_huge_operations = True
    spectrum = cube.sum(axis=(1, 2))  
    cube.allow_huge_operations = False
    if save : 
        spectrum.write(file)
    return spectrum    


def Rest_spectrum(wave,flux,redshift):
    """de-Redshift template spectrum
    """ 
    lnlambdagal = np.log10(wave)
    ln_shift = np.log10(1. + redshift)
    der_lnlambdagal = lnlambdagal  - ln_shift
    wave_rest = 10**(der_lnlambdagal)
    flux_rest = flux*(1+redshift) 
    return wave_rest, flux_rest  
 

def Line_models2properties(fitted_model, wave, flux):
    
    #Evaluate the noise
    residual = flux - fitted_model(wave)
    lines_model = fitted_model[0:-1]
    lines_region = np.where(lines_model(wave) > 1E-10)
    noise = np.nanstd(residual[np.invert(lines_region)])
    
    lines = []
    
    for line in lines_model:
        flux_model = line.stddev.value * line.amplitude.value * np.sqrt(2 * math.pi)
        ind_line = np.squeeze(np.where((wave > line.mean.value - 3* line.stddev.value) & (wave < line.mean.value + 3 * line.stddev.value) ))
        flux_sub = flux - fitted_model['Continum'](wave)
        flux_int = np.trapz(x = wave[ind_line], y = flux_sub[ind_line])
        snr = flux_model / ( noise * np.sqrt(ind_line.shape[0])) 
        flag = 1
        
        if (flux_model == 0) or (flux_int == 0):
            flag = -3
        
        elif (flux_model / flux_int > 1.2) or (flux_model / flux_int < 0.8):
            flag = -1
        line = {'name': line.name , 'centre' : line.mean.value  , 'sigma' : line.stddev.value, 'flux_int' : flux_int ,'flux_model' : flux_model, 'snr' : snr, 'flag' : flag }
       # print(line)
        lines.append(line)
    return lines

def Line_2properties(fitted_model, wave, flux):
    
    #Evaluate the noise
    residual = flux - fitted_model(wave)
    lines_model = fitted_model[0:-1]
    lines_region = np.where(lines_model(wave) > 1E-10)
    noise = np.nanstd(residual[np.invert(lines_region)])

    lines = []
    
    flux_model = lines_model.stddev.value * lines_model.amplitude.value * np.sqrt(2 * math.pi)
    ind_line = np.squeeze(np.where((wave > lines_model.mean.value - 3* lines_model.stddev.value) & (wave < lines_model.mean.value + 3 * lines_model.stddev.value) ))
    flux_sub = flux - fitted_model['Continum'](wave)
    flux_int = np.trapz(x = wave[ind_line], y = flux_sub[ind_line])
    snr = flux_model / ( noise * np.sqrt(ind_line.shape[0])) 
    flag = 1   
    if (flux_model == 0) or (flux_int == 0):
        flag = -3
        
    elif (flux_model / flux_int > 1.2) or (flux_model / flux_int < 0.8):
        flag = -1
    line = {'name': lines_model.name , 'centre' : lines_model.mean.value  , 'sigma' : lines_model.stddev.value, 'flux_int' : flux_int ,'flux_model' : flux_model, 'snr' : snr, 'flag' : flag }
    lines.append(line)
    return lines


#To be remove

#def Fit_line(wave_,flux_, lambda_line = 4861.3, name = ['Hb'], region_bounds = [80,80], stddev_bounds = [0,10], mean_bounds = [-5,5]):
#    return fitted_model, lines
    
def Fit_line(wave_,flux_, lambda_line = 4861.3, name = ['Hb'], region_bounds = [80,80], stddev_bounds = [0.,10.], mean_bounds = [-5,5]):
    
    Doublet_region = [lambda_line - region_bounds[0] ,lambda_line + region_bounds[1] ]
    ind = np.where((wave_ >= Doublet_region[0]) & (wave_ < Doublet_region[1]))
    wave = wave_[ind]
    flux = flux_[ind]

    # Emission lines are always positive
    max_ampl = np.max(flux)
    max_index = np.where(flux == max_ampl)[0]
    # Compute first guess for gaussian params    
    # Create Gaussian1D models for each line
    d_1 = models.Gaussian1D(amplitude = max_ampl, mean = lambda_line, stddev = 2.5, name = name)
    
    # Boundaries for stddev
    d_1.stddev.bounds = stddev_bounds

    # Emission lines are always positive
    d_1.mean.bounds = [lambda_line + mean_bounds[0], lambda_line + mean_bounds[1]]
    
    # Create a Polynomial model to fit the continuum.
    mean_flux = flux.mean()
    cont = np.where(flux > mean_flux, mean_flux, flux)
    linfitter = fitting.LinearLSQFitter()
    poly_cont = linfitter(models.Polynomial1D(1, name='Continum'), wave, cont)

    # Create a compound model for the three lines and the continuum.
    single_model = d_1  + poly_cont

    # Fit all lines simultaneously.
    fitter = fitting.LevMarLSQFitter()
    fitted_model = fitter(single_model, wave, flux)
    lines = Line_2properties(fitted_model, wave, flux)
    
    return fitted_model, lines


    
    
def Fit_Doublet(wave_,flux_, double_lambda = [5006.85, 4958.911], doublet_name = ['[OIII]5007','[OIII]4959'], region_bounds = [80,80], stddev_bounds = [0.01,10], tied_amp = False, ratio_amp = 3.1):
    # Assumes that the strongest line is the first in the list
    # Use the rest wavelengths of known lines as initial values for the fit.
    
    #Isolate the region around the OIII doublet
    Doublet_region = [min(double_lambda) - 80 ,max(double_lambda) + 80 ]
    ind = np.where((wave_ >= Doublet_region[0] - region_bounds[0]) & (wave_ < Doublet_region[1] + region_bounds[1]))
    wave = wave_[ind]
    flux = flux_[ind]
    max_ampl = np.max(flux)
    
    # Compute first guess for gaussian params    
    # Create Gaussian1D models for each line
    d_1 = models.Gaussian1D(amplitude = max_ampl, mean = double_lambda[0], stddev = 2.5, name = doublet_name[0])
    d_2 =  models.Gaussian1D(amplitude = max_ampl/2., mean = double_lambda[1], stddev = 2.5, name = doublet_name[1])

    # Boundaries for position
    d_1.mean.bounds = [double_lambda[0] - 10,double_lambda[0] + 10]
        
    # Boundaries for stddev
    d_1.stddev.bounds = stddev_bounds
    
    # Emission lines are always positive
    
   # d_1.amplitude.bounds = [0,max_ampl]
    d_2.amplitude.bounds = [0,max_ampl]

    # Tie the ratio of the intensity of the two lines (gaussian two is the weakest line) 
    def tie_ampl(model):
        return model.amplitude_0 / ratio_amp
    if tied_amp : 
        d_2.amplitude.tied = tie_ampl
    
    # Also tie the wavelength of the stronest line to the weakest
    def tie_wave(model):
        return model.mean_0 - (double_lambda[0] - double_lambda[1])
    d_2.mean.tied = tie_wave

    # tie the sigma in the doublet
    def tie_sigma(model):
        return model.stddev_0    
    d_2.stddev.tied = tie_sigma

    # Create a Polynomial model to fit the continuum.
    mean_flux = flux.mean()
    cont = np.where(flux > mean_flux, mean_flux, flux)
    linfitter = fitting.LinearLSQFitter()
    poly_cont = linfitter(models.Polynomial1D(1, name='Continum'), wave, cont)

    # Create a compound model for the three lines and the continuum.
    doublet_model = d_1 + d_2 + poly_cont

    # Fit all lines simultaneously.
    fitter = fitting.LevMarLSQFitter()
    fitted_model = fitter(doublet_model, wave, flux)
    lines = Line_models2properties(fitted_model, wave, flux)
    
    return fitted_model, lines

  
    
def Fit_HaNIICombo(wave_,flux_):
        #Add code here
    
    ##### ERASE EVERYTHING HERE
    # Use the rest wavelengths of known lines as initial values for the fit.
    NII_1 = 6548.1
    NII_2 = 6583.4
    Haplha = 6562.8
    Delta_NII = NII_2 - NII_1
    
    #Isolate the region around the OIII doublet
    HaNII_region = [NII_1 - 80 ,NII_2 + 80 ]
    ind = np.where((wave_ >= HaNII_region[0]) & (wave_ < HaNII_region[1]))
    wave = wave_[ind]
    flux = flux_[ind]
    max_ampl = np.max(flux)
    
    # Create Gaussian1D models for each of OIII lines
    n2_1 = models.Gaussian1D(amplitude=max_ampl/2., mean=NII_1, stddev=2.5, name='[NII]6716')
    n2_2 = models.Gaussian1D(amplitude=max_ampl/3., mean=NII_2, stddev=2.5, name='[NII]6562')
    Ha = models.Gaussian1D(amplitude=max_ampl, mean=Haplha, stddev=2.5, name='Ha')

    # Boundaries for position+/- 5A around the reference wavelength
    #n2_1.mean.bounds = [NII_1 - 5,NII_1 + 5]
    #n2_2.mean.bounds = [NII_2 - 5,NII_2 + 5]
    #Ha.mean.bounds = [Haplha - 5,Haplha + 5]
    
    # Boundaries for stddev
    #n2_1.stddev.bounds = [1,10]
    #n2_2.stddev.bounds = [1,10]
    
    # Emission lines are always positive
    #n2_1.amplitude.bounds = [0,max_ampl]
   # n2_2.amplitude.bounds = [0,max_ampl]
   # Ha.amplitude.bounds = [0,max_ampl]

    # Also tie the wavelength of the NII.2 line to NII.1 the wavelength.
    def tie_wave(model):
        return model.mean_2 - Delta_NII
    n2_1.mean.tied = tie_wave

    # tie the sigma in the NII doublet
    def tie_sigma(model):
        return model.stddev_2    
    n2_1.stddev.tied = tie_sigma
    
    # Create a Polynomial model to fit the continuum.
    mean_flux = flux.mean()
    cont = np.where(flux > mean_flux, mean_flux, flux)
    linfitter = fitting.LinearLSQFitter()
    poly_cont = linfitter(models.Polynomial1D(1, name='Continum'), wave, cont)

    # Create a compound model for the three lines and the continuum.
    HaNII_doublet = n2_1 + Ha + n2_2 + poly_cont

    # Fit all lines simultaneously.
    fitter = fitting.LevMarLSQFitter()
    fitted_model = fitter(HaNII_doublet, wave, flux)
    
    ierr = fitter.fit_info['ierr'] 
    lines = Line_models2properties(fitted_model, wave, flux)
    
    return fitted_model, lines
    
def SaveMaps(Maps,name='Halpha',fileout='ngc7468_Halpha.fits'):
    hdu = fits.PrimaryHDU()
    hdu.header['NAME']= name
    hdu1 = fits.ImageHDU(Maps[:,:,0])
    hdu1.header['NAME']='Vel'
    hdu2 = fits.ImageHDU(Maps[:,:,1])
    hdu2.header['NAME']='Sigma'
    hdu3 = fits.ImageHDU(Maps[:,:,2])
    hdu3.header['NAME']='Flux_m'
    hdu4 = fits.ImageHDU(Maps[:,:,3])
    hdu4.header['NAME']='Flux_i'
    hdu5 = fits.ImageHDU(Maps[:,:,4])
    hdu5.header['NAME']='SNR'
    hdu6 = fits.ImageHDU(Maps[:,:,5])
    hdu6.header['NAME']='Flag'
    new_hdul = fits.HDUList([hdu,hdu1,hdu2,hdu3,hdu4,hdu5,hdu6])
    new_hdul.writeto(fileout, overwrite=True)
            
        
def BPT_OIIIHb_NIIHa_class(ratio_NIIHa, ratio_OIIIHb, plot = False): 
    
    n = len(ratio_NIIHa)
    BTP_class = np.empty(n , dtype="S10")
    
    for i in range(n):    
        BTP_class[i] = 'None'
        if (ratio_NIIHa[i] == np.nan ) | (ratio_OIIIHb[i] == np.nan ): 
            BTP_class[i] = np.nan    
        if (0.61 / (ratio_NIIHa[i]  - 0.05) + 1.3 > ratio_OIIIHb[i]):
            BTP_class[i] = 'SF'
        if (0.61 / (ratio_NIIHa[i] - 0.05) + 1.3 < ratio_OIIIHb[i]) & (0.61 / (ratio_NIIHa[i] - 0.47) + 1.19 > ratio_OIIIHb[i]):
            BTP_class[i] = 'Composite'    
        if (ratio_OIIIHb[i] > 0.61/(ratio_NIIHa[i] - 0.47) + 1.19) | (ratio_NIIHa[i] >= 0.47): 
            BTP_class[i] = 'AGN'    
        
    if plot : 
        AGN_index = np.where(BTP_class==b'AGN')
        Composite_index = np.where(BTP_class==b'Composite')
        SF_index = np.where(BTP_class==b'SF')
        None_index = np.where(BTP_class==b'None') 
        
        color = np.array(["black "]*n)
        color[AGN_index] = 'red' #'AGN'
        color[Composite_index] = 'green' #'Composite'
        color[SF_index] = 'blue' #'SF'
        color[None_index] = 'yellow' #'None'
        
        x_AGN = np.arange(-2,0.47 ,0.01)
        y_AGN =  0.61/(x_AGN - 0.47) + 1.19 
        x_composite = np.arange(-1.2,-0.01,0.01)
        y_composite =  0.61/(x_composite - 0.05) + 1.3 
        
        fig = plt.figure(figsize=(6, 6))
        p = plt.plot(x_AGN, y_AGN, label="AGN")
        p = plt.plot(x_composite, y_composite, 'b-', label="Composite")
        p = plt.scatter(ratio_NIIHa, ratio_OIIIHb, color=color)
        p = plt.legend()
        p = plt.xlabel("log(NII/Ha)")
        p = plt.ylabel("log([OIII]/Hb)")  
        plt.xlim(-2,0.8)
        plt.ylim(-1.2,1.5)
        plt.show()

    return BTP_class

def BPT_OIIIHb_SIIHa_class(ratio_SIIHa, ratio_OIIIHb, plot = False):     
    n = len(ratio_OIIIHb)
    BTP_class = np.empty(n , dtype="S10")
    
    for i in range(n):
        BTP_class[i] = 'None'
        if (ratio_OIIIHb[i] == np.nan ) | (ratio_SIIHa[i] == np.nan):
            BTP_class[i] = np.nan
        if (ratio_OIIIHb[i] < 0.72 /(ratio_SIIHa[i]  - 0.32) + 1.3):
            BTP_class[i] = 'SF'  
        if ((0.72 / (ratio_SIIHa[i] - 0.32) + 1.3 < ratio_OIIIHb[i]) | (ratio_SIIHa[i] >= 0.32)):
            if (1.89 * ratio_SIIHa[i] + 0.76 < ratio_OIIIHb[i]):
                BTP_class[i] = 'Seyfert'    
            if (1.89 * ratio_SIIHa[i] + 0.76 >= ratio_OIIIHb[i]):
                BTP_class[i] = 'Liner' 
    if plot :

        x_SF = np.arange(-1.1,0.32 ,0.01)
        y_SF =  0.72/(x_SF - 0.32) + 1.3
        x_Liner = np.arange(-0.3,0.05,0.01)
        y_Liner =  1.89*x_Liner  + 0.76 
        fig = plt.figure(figsize=(6, 6))
        p = plt.plot(x_SF, y_SF, label="SF")
        p = plt.plot(x_Liner, y_Liner, 'b-', label="Seyfert/Liners")  
        p = plt.scatter(ratio_SIIHa,ratio_OIIIHb)
        p = plt.legend()
        p = plt.xlabel("log(SII/Ha)")
        p = plt.ylabel("log([OIII]/Hb)")  
        plt.xlim(-1.2,0.8)
        plt.ylim(-1.2,1.5)
        plt.show()
    return BTP_class

def BPT_all_class(BTP_OIIINII_class, BTP_OIIISII_class, Flag = False):  
    n = len(BTP_OIIINII_class)
    BTP_class = np.empty(n , dtype="S10")
    
    for i in range(n):
        
        BTP_class[i] = 'Ambiguous' 
        if Flag : 
                BTP_class[i] = 5
        if (BTP_OIIINII_class[i] == np.nan) & (BTP_OIIISII_class[i] == np.nan):        
            BTP_class[i] = np.nan
        if (BTP_OIIINII_class[i] == 'SF') & (BTP_OIIISII_class[i] == 'SF'):
            BTP_class[i] = 'SF'
            if Flag : 
                BTP_class[i] = 1
        if (BTP_OIIINII_class[i] == 'Composite') & (BTP_OIIISII_class[i] == 'SF'):
            BTP_class[i] = 'Composite'
            if Flag : 
                BTP_class = 2
        if (BTP_OIIINII_class[i] == 'AGN') & (BTP_OIIISII_class[i] == 'Seyfert'):
            BTP_class[i] = 'Seyfert' 
            if Flag : 
                BTP_class[i] = 3
        if (BTP_OIIINII_class == 'AGN') & (BTP_OIIISII_class == 'Liner'):
            BTP_class = 'Liner'
            if Flag : 
                BTP_class[i] = 4
        if (BTP_OIIINII_class[i] == 'Composite') & (BTP_OIIISII_class[i] == 'Seyfert'):
            BTP_class[i] = 'Seyfert'  
            if Flag : 
                BTP_class[i] = 3
        if (BTP_OIIINII_class[i] == 'Composite') & (BTP_OIIISII_class[i] == 'Liner'):
            BTP_class[i] = 'Liner'
            if Flag : 
                BTP_class[i] = 4
        return BTP_class
